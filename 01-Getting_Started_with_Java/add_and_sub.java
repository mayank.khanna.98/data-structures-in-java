class Test {
    static int add(int num1, int num2) {
        return num1+num2;
    }

    static int sub(int num1, int num2) {
        return num1-num2;
    }

    public static void main(String[] args) {
        System.out.println("Addition of 40 and 20: " + add(40, 20));
        System.out.println("Subtraction of 40 and 20: " + sub(40, 20));
    }
}