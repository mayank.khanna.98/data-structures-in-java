# 01 - Getting Started with Java

- This is a simple repo where I will try and practice and learn Java programming language.
- This folder will contain all the basic programs and the problems I will solve.
- Anyone who wishes to learn Java or revise the concepts of Java Programming language can refer to this repo.
- This will help all level programmers i.e from Novice to Pro.
