# Practicing and Learning Data Structures using Java

- This is a repository for learning and practicing Data Structures using Java Language.
- This repo will contain all kinds of problems and questions i.e from Beginner level to Pro Data Structures.
- This is a repo for my practice and if anyone who wishes to learn Java basics or Data Structures can refer to this repo.
- I hope this repo will help me and any person learning from this grow and excel.

> *Change your life today. Don't gamble on the future, act now, without delay.* :100:

- Happy Coding :grin:
